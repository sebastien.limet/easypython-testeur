__author__ = 'julien'
from ..Testeur import Testeur
from string import Template
import os
import shutil



class TesteurMYSQL(Testeur):
    
    nom = "mysql"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def writeTestFiles(self, directory):

        with open(os.path.join(directory, "enonce_enseignant.yaml"), "w", encoding="utf-8") as yaml_test:
            yaml_test.write(self.codeTest.decode())

        with open(os.path.join(directory, "solution_etudiant.yaml"), "w", encoding="utf-8") as solution_etudiant:
            solution_etudiant.write(self.codeATester)
        
        with open(os.path.join(directory, "makefile"), "w", encoding="utf-8") as file:
            file.write("all:\n\tpython3 testSolution.py solution_etudiant.yaml enonce_enseignant.yaml\n\ninfos:\n\techo {}")

        shutil.copyfile(os.path.join(os.path.dirname(__file__), "testSolution.py"),
                        os.path.join(directory, "testSolution.py"))
